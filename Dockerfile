FROM dockcross/base:latest
RUN apt update && apt install -y freeglut3-dev libglew-dev
COPY . /home/opengl/
WORKDIR /home/opengl/OpenGL/OpenGL/
RUN g++ Source.cpp -v -lGL -lglut -lGLEW
