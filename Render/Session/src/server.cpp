#include"../headers/server.h"

Server::~Server()
{
}

Server::Server(IoService& t_ioService, short t_port, std::string const& t_workDirectory)
	: m_socket(t_ioService),
	m_acceptor(t_ioService, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), t_port)),
	m_workDirectory(t_workDirectory)
{
	std::cout << "Server started\n";

	m_path = t_workDirectory;

	createWorkDirectory();

	doAccept();
}

int index = 0;

void Server::doAccept()
{
	m_acceptor.async_accept(m_socket,
		[this](boost::system::error_code ec)
	{
		if (!ec)
			std::make_shared<Session>(std::move(m_socket))->start();
		std::cout << "Sunt in doaccespt si am index = "<<++index<<std::endl;
		showImages::closeCurrentWindow();
		showImages::show(getWorkDirectory());
		doAccept();
	});
}


void Server::createWorkDirectory()
{
	using namespace boost::filesystem;
	auto currentPath = path(m_workDirectory);
	if (!exists(currentPath) && !create_directory(currentPath))
		BOOST_LOG_TRIVIAL(error) << "Coudn't create working directory: " << m_workDirectory;
	current_path(currentPath);
}