#include"../headers/Session.h"

Session::Session(TcpSocket t_socket)
	: m_socket{ std::move(t_socket)},
		m_fileSize{0},
		m_buf{'x',0}
{
}

void Session::start()
{
	doRead();
}

void Session::doRead()
{
	auto self = shared_from_this();
	async_read_until(m_socket, m_requestBuf_, "\n\n",
		[this, self](boost::system::error_code ec, size_t bytes)
	{
		if (!ec) {
			processRead(bytes);
		}
		else
			handleError(__FUNCTION__, ec);
	});
}

void Session::processRead(size_t t_bytesTransferred)
{
	BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ << "(" << t_bytesTransferred << ")"
		<< ", in_avail = " << m_requestBuf_.in_avail() << ", size = "
		<< m_requestBuf_.size() << ", max_size = " << m_requestBuf_.max_size() << ".";

	std::istream requestStream(&m_requestBuf_);
	readData(requestStream);

	auto pos = m_fileName.find_last_of('\\');
	if (pos != std::string::npos)
		m_fileName = m_fileName.substr(pos + 1);

	//just for test
	std::string ipAddres = m_socket.remote_endpoint().address().to_string();
	unsigned int portNo = m_socket.remote_endpoint().port();

	//connectedUsers.emplace(std::pair<std::string, unsigned int>(ipAddres, portNo));

	//std::cout << "New client connected : " << ipAddres << " | " << portNo << std::endl;
	//std::cout << "Client size : " << connectedUsers.size();

	createFile();

	// write extra bytes to file
	do {
		requestStream.read(m_buf.data(), m_buf.size());
		BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ << " write " << requestStream.gcount() << " bytes.";
		m_outputFile.write(m_buf.data(), requestStream.gcount());
	} while (requestStream.gcount() > 0);

	auto self = shared_from_this();
	m_socket.async_read_some(boost::asio::buffer(m_buf.data(), m_buf.size()),
		[this, self](boost::system::error_code ec, size_t bytes)
	{
		if (!ec)
			doReadFileContent(bytes);
		else
			handleError(__FUNCTION__, ec);
	});
}

void Session::readData(std::istream &stream)
{

	stream >> m_fileName;
	stream >> m_fileSize;
	stream.read(m_buf.data(), 2);

	BOOST_LOG_TRIVIAL(trace) << m_fileName << " size is " << m_fileSize
		<< ", tellg = " << stream.tellg();
}

void Session::createFile()
{
	m_outputFile.open(m_fileName, std::ios_base::binary);
	if (!m_outputFile) {
		BOOST_LOG_TRIVIAL(error) << __LINE__ << ": Failed to create: " << m_fileName;
		return;
	}
}

void Session::doReadFileContent(size_t t_bytesTransferred)
{
	if (t_bytesTransferred > 0) {
		m_outputFile.write(m_buf.data(), static_cast<std::streamsize>(t_bytesTransferred));

		BOOST_LOG_TRIVIAL(trace) << __FUNCTION__ << " recv " << m_outputFile.tellp() << " bytes";

		if (m_outputFile.tellp() >= static_cast<std::streamsize>(m_fileSize)) {
			std::cout << "Received file: " << m_fileName << std::endl;
			return;
		}
	}
	auto self = shared_from_this();
	m_socket.async_read_some(boost::asio::buffer(m_buf.data(), m_buf.size()),
		[this, self](boost::system::error_code ec, size_t bytes)
	{
		doReadFileContent(bytes);
	});
}


void Session::handleError(std::string const& t_functionName, boost::system::error_code const& t_ec)
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << " in " << t_functionName << " due to "
		<< t_ec << " " << t_ec.message() << std::endl;
}