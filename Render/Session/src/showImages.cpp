#include"../headers/showImages.h"

int showImages::index = 0;
std::string showImages::windowName = "";
std::atomic_bool showImages::flag = true;
using namespace boost::filesystem;

void doWork(string windowName, vector<Mat> nArgs) {
	showImages::windowName = windowName;
	int size;
	int i;
	int m, n;
	int x, y;

	// w - Maximum number of images in a row
	// h - Maximum number of images in a column
	int w, h;

	// scale - How much we have to resize the image
	float scale;
	int max;

	// If the number of arguments is lesser than 0 or greater than 12
	// return without displaying
	if (nArgs.size() <= 0) {
		printf("Number of arguments too small....\n");
		return;
	}
	else if (nArgs.size() > 14) {
		printf("Number of arguments too large, can only handle maximally 12 images at a time ...\n");
		return;
	}
	// Determine the size of the image,
	// and the number of rows/cols
	// from number of arguments
	else if (nArgs.size() == 1) {
		w = h = 1;
		size = 300;
	}
	else if (nArgs.size() == 2) {
		w = 2; h = 1;
		size = 300;
	}
	else if (nArgs.size() == 3 || nArgs.size() == 4) {
		w = 2; h = 2;
		size = 300;
	}
	else if (nArgs.size() == 5 || nArgs.size() == 6) {
		w = 3; h = 2;
		size = 200;
	}
	else if (nArgs.size() == 7 || nArgs.size() == 8) {
		w = 4; h = 2;
		size = 200;
	}
	else {
		w = 4; h = 3;
		size = 150;
	}

	// Create a new 3 channel image
	Mat DispImage = Mat::zeros(Size(100 + size * w, 60 + size * h), CV_8UC3);

	// Used to get the arguments passed
	/*va_list args;
	va_start(args, nArgs);*/

	// Loop for nArgs number of arguments
	for (i = 0, m = 20, n = 20; i < nArgs.size(); i++, m += (20 + size)) {
		// Get the Pointer to the IplImage
		Mat img = nArgs[i];

		// Check whether it is NULL or not
		// If it is NULL, release the image, and return
		if (img.empty()) {
			printf("Invalid arguments");
			return;
		}

		// Find the width and height of the image
		x = img.cols;
		y = img.rows;

		// Find whether height or width is greater in order to resize the image
		max = (x > y) ? x : y;

		// Find the scaling factor to resize the image
		scale = (float)((float)max / size);

		// Used to Align the images
		if (i % w == 0 && m != 20) {
			m = 20;
			n += 20 + size;
		}

		// Set the image ROI to display the current image
		// Resize the input image and copy the it to the Single Big Image
		Rect ROI(m, n, (int)(x / scale), (int)(y / scale));
		Mat temp; resize(img, temp, Size(ROI.width, ROI.height));
		temp.copyTo(DispImage(ROI));
	}

	// Create a new window, and show the Single Big Image
	namedWindow(windowName, 1);
	imshow(windowName, DispImage);
	waitKey();
}

void showImages::devideImages(string windowName, vector<Mat> nArgs)
{
	boost::thread t{&doWork ,windowName,nArgs};
	t.detach();
}

void showImages::closeCurrentWindow()
{
	if (showImages::windowName.size() != 0)
		destroyWindow(showImages::windowName);
	std::cout << "I have distroyd window " << showImages::windowName<<std::endl;
}

void showImages::show(std::string strPath)
{
	std::cout << strPath << std::endl;

	path p(strPath);

	directory_iterator end_itr;
	vector<Mat> images;

	// cycle through the directory
	for (directory_iterator itr(p); itr != end_itr; ++itr)
	{
		// If it's not a directory, list it. If you want to list directories too, just remove this check.
		if (is_regular_file(itr->path())) {
			// assign current file name to current_file and echo it out to the console.

			boost::filesystem::path fe = itr->path().extension();
			std::cout << fe.string() << std::endl;
			std::cout << "---------------->" << fe << "<----------------" << std::endl;

			if (".png" == fe)
			{
				string current_file = itr->path().string();
				cout << current_file << endl;
				boost::replace_all(current_file, "/", "\\");
				images.push_back(imread(current_file));
			}
			else
				std::cout << "[FILE] " << itr->path().string() << " is not a png image format." << std::endl;
		}
	}
	devideImages(windowName, images);

}