#pragma once

#include <array>
#include <fstream>
#include <string>
#include <memory>
#include <boost/asio.hpp>

#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>
#include <boost/asio/read_until.hpp>

#include <boost/asio/io_service.hpp>
#include <boost/thread.hpp>

#include <iostream>
#include <memory>

#include"../headers/Session.h"
#include"../headers/showImages.h"
#include"../../Common/logger.h"


class Session;

#ifdef IMPORT 
#define GUI_CTRLS_EXPORT __declspec(dllexport) 
#else 
#define EXPORT  __declspec(dllimport) 
#endif 

class Server
{
public:
	using TcpSocket = boost::asio::ip::tcp::socket;
	using TcpAcceptor = boost::asio::ip::tcp::acceptor;
	using IoService = boost::asio::io_service;

	EXPORT Server(IoService& t_ioService, short t_port, std::string const& t_workDirectory);
	EXPORT ~Server();

	//private:
	EXPORT void doAccept();
	EXPORT void createWorkDirectory();
	std::string getWorkDirectory() { return m_workDirectory; };


	TcpSocket m_socket;
	TcpAcceptor m_acceptor;
	std::string m_path;

	std::string m_workDirectory;
};