#pragma once
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/gapi/own/scalar.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include<boost/thread.hpp>
#include<vector>
#include<atomic>
#include "boost/filesystem.hpp"
#include <boost/algorithm/string.hpp>

using std::string;
using std::cout;
using std::endl;
using std::vector;

using namespace cv;

class showImages
{
public:
	static void show(std::string path);
	static void devideImages(string title, vector<Mat> nArgs);
	static int index;
	static string windowName;
	static void closeCurrentWindow();
	static std::atomic_bool flag;

};

