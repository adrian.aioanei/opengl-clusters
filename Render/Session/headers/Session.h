#pragma once
#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <memory>

#include <boost/asio.hpp>
#include <boost/log/trivial.hpp>
#include <boost/asio/read_until.hpp>

#ifdef IMPORT 
#define GUI_CTRLS_EXPORT __declspec(dllexport) 
#else 
#define EXPORT  __declspec(dllimport) 
#endif 

class Session
	: public std::enable_shared_from_this<Session>
{
public:
	using TcpSocket = boost::asio::ip::tcp::socket;

	EXPORT Session(TcpSocket t_socket);

	void EXPORT start();

//private:
	void EXPORT doRead();
	void EXPORT processRead(size_t t_bytesTransferred);
	void EXPORT createFile();
	void EXPORT readData(std::istream &stream);
	void EXPORT doReadFileContent(size_t t_bytesTransferred);
	void EXPORT handleError(std::string const& t_functionName, boost::system::error_code const& t_ec);


	TcpSocket m_socket;
	enum { MaxLength = 40960 };
	std::array<char, MaxLength> m_buf;
	boost::asio::streambuf m_requestBuf_;
	std::ofstream m_outputFile;
	size_t m_fileSize;
	std::string m_fileName;
};
