#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <array>

#include <boost/filesystem/path.hpp>
#include <boost/log/trivial.hpp>
#include <boost/asio.hpp>

#include"../headers/Session.h"

#ifdef IMPORT 
#define GUI_CTRLS_EXPORT __declspec(dllexport) 
#else 
#define EXPORT  __declspec(dllimport) 
#endif 

class Client
{
public:
	using IoService = boost::asio::io_service;
	using TcpResolver = boost::asio::ip::tcp::resolver;
	using TcpResolverIterator = TcpResolver::iterator;
	using TcpSocket = boost::asio::ip::tcp::socket;


	EXPORT Client(IoService& t_ioService, TcpResolverIterator t_endpointIterator,
		std::string const& t_path);

	EXPORT ~Client();


	EXPORT void openFile(std::string const& t_path);
	EXPORT void doConnect();
	EXPORT void doWriteFile(const boost::system::error_code& t_ec);
	template<class Buffer>
	void writeBuffer(Buffer& t_buffer);

	TcpResolver m_ioService;
	TcpSocket m_socket;
	TcpResolverIterator m_endpointIterator;
	enum { MessageSize = 1024 };
	std::array<char, MessageSize> m_buf;
	boost::asio::streambuf m_request;
	std::ifstream m_sourceFile;
	std::string m_path;
};


template<class Buffer>
void Client::writeBuffer(Buffer& t_buffer)
{
	boost::asio::async_write(m_socket,
		t_buffer,
		[this](boost::system::error_code ec, size_t /*length*/)
	{
		doWriteFile(ec);
	});
}