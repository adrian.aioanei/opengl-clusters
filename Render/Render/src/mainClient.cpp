#include <iostream>
#include <string>
#include<vector>

#include <boost/asio/io_service.hpp>

#include "../../Session/headers/client.h"
#include "../../Common/logger.h"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/gapi/own/scalar.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

using std::string;
using std::cout;
using std::endl;
using std::vector;
namespace ip = boost::asio::ip;

using namespace cv;

string RandomString(int len)
{
	srand(time(0));
	string str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	string newstr;
	int pos;
	while (newstr.size() != len) {
		pos = ((rand() % (str.size() - 1)));
		newstr += str.substr(pos, 1);
	}
	return newstr;
}

void createAlphaMat(Mat &mat)
{
	for (int i = 0; i < mat.rows; ++i) {
		for (int j = 0; j < mat.cols; ++j) {
			Vec4b& rgba = mat.at<Vec4b>(i, j);
			rgba[0] = UCHAR_MAX;
			rgba[1] = saturate_cast<uchar>((float(mat.cols - j)) / ((float)mat.cols) * UCHAR_MAX);
			rgba[2] = saturate_cast<uchar>((float(mat.rows - i)) / ((float)mat.rows) * UCHAR_MAX);
			rgba[3] = saturate_cast<uchar>(0.5 * (rgba[1] + rgba[2]));
		}
	}
}

int modelOne(std::string filePath) {
	Mat mat(480, 640, CV_8UC4);
	createAlphaMat(mat);

	vector<int> compression_params;
	compression_params.push_back(IMWRITE_PNG_COMPRESSION);
	compression_params.push_back(9);

	try {
		imwrite(filePath, mat, compression_params);
	}
	catch (std::runtime_error& ex) {
		fprintf(stderr, "Exception converting image to PNG format: %s\n", ex.what());
		return 1;
	}

	fprintf(stdout, "Saved PNG file with alpha data.\n");
	return 0;
}


int modelTwo(std::string filePath) {
	Mat image;
	image = imread("data/puppy.png", IMREAD_COLOR);
	if (!image.data)
	{
		printf(" No image data \n ");
		return -1;
	}
	Mat gray_image;
	cvtColor(image, gray_image, COLOR_BGR2GRAY);
	imwrite(filePath, gray_image);
	return 0;
}


int main(int argc, char* argv[])
{
	if (argc != 3) {
		std::cerr << "Usage: ./client <server_address>\n";
		return 1;
	}
	auto address = argv[1];
	auto ch = argv[2];
	auto filePath = RandomString(20) + ".png";

	if ('0' == *ch)
		auto rez = modelOne(filePath);
	else 
		auto rez = modelTwo(filePath);

	Logger::instance().setOptions("client_%3N.log", 1 * 1024 * 1024, 10 * 1024 * 1024);

	try {
		boost::asio::io_service ioService;
		 
		boost::asio::ip::tcp::resolver resolver(ioService);
		auto endpointIterator = resolver.resolve(ip::tcp::endpoint(ip::address::from_string(argv[1]), 22222));
		Client client(ioService, endpointIterator, filePath);

		ioService.run();
	}
	catch (std::fstream::failure& e) {
		std::cerr << e.what() << "\n";
	}
	catch (std::exception& e) {
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}