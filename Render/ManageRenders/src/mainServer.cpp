#include "../../Session/headers/server.h"

int main(int argc, char* argv[])
{
	std::cout << argc;

	std::cout <<" "<< argv[0];
	std::cout <<" "<< argv[1];

	if (argc != 2) {
		std::cerr << "Usage: ./server  <workDirectory>\n";
		return 1;
	}

	Logger::instance().setOptions("server_%3N.log", 1 * 1024 * 1024, 10 * 1024 * 1024);

	try {
		boost::asio::io_service ioService;
		Server server(ioService, 22222, argv[1]);
		ioService.run();

	}
	catch (std::exception& e) {
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}